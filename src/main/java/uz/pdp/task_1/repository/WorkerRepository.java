package uz.pdp.task_1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.task_1.entity.Department;
import uz.pdp.task_1.entity.Worker;

public interface WorkerRepository extends JpaRepository<Worker,Integer> {
}
