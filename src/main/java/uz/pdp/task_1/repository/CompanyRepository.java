package uz.pdp.task_1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.task_1.entity.Address;
import uz.pdp.task_1.entity.Company;

public interface CompanyRepository extends JpaRepository<Company,Integer> {
}
