package uz.pdp.task_1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.task_1.entity.Address;

public interface AddressRepository extends JpaRepository<Address,Integer> {

}
