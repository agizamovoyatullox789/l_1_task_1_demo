package uz.pdp.task_1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.task_1.entity.Company;
import uz.pdp.task_1.entity.Department;

public interface DepartmentRepository extends JpaRepository<Department,Integer> {
}
