package uz.pdp.task_1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task_1.entity.Address;
import uz.pdp.task_1.entity.Worker;
import uz.pdp.task_1.payload.ApiResponse;
import uz.pdp.task_1.payload.WorkerDto;
import uz.pdp.task_1.repository.AddressRepository;
import uz.pdp.task_1.repository.DepartmentRepository;
import uz.pdp.task_1.repository.WorkerRepository;

import java.util.List;
import java.util.Optional;

@Service
public class WorkerService {
    @Autowired
    WorkerRepository workerRepository;
    @Autowired
    AddressRepository addressRepository;
    @Autowired
    DepartmentRepository departmentRepository;
    public List<Worker> getWorkers() {
        return workerRepository.findAll();
    }

    public Worker getWorker(Integer id) {
        Optional<Worker> byId = workerRepository.findById(id);
        return byId.orElse(null);
    }

    public ApiResponse addWorker(WorkerDto workerDto) {
        Worker worker=new Worker(
                workerDto.getName(),
                workerDto.getPhoneNumber(),
                addressRepository.getById(workerDto.getAddressId()),
                departmentRepository.getById(workerDto.getDepartmentId())
        );
        workerRepository.save(worker);
        return new ApiResponse("Worker added", true);
    }

    public ApiResponse deleteWorker(Integer id) {
        workerRepository.deleteById(id);
        return new ApiResponse("Worker deleted",true);
    }

    public ApiResponse editWorker(Integer id, WorkerDto workerDto) {
        Worker worker= workerRepository.findById(id).orElseThrow(() -> new IllegalStateException("Worker not found"));
        worker.setName(workerDto.getName());
        worker.setPhoneNumber(workerDto.getPhoneNumber());
        worker.setAddress(addressRepository.getById(workerDto.getAddressId()));
        worker.setDepartment(departmentRepository.getById(workerDto.getDepartmentId()));
        workerRepository.save(worker);
        return new ApiResponse("Worker edited",true);
    }
}
