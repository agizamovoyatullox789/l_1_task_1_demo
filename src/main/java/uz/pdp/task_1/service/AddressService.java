package uz.pdp.task_1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import uz.pdp.task_1.entity.Address;
import uz.pdp.task_1.payload.ApiResponse;
import uz.pdp.task_1.repository.AddressRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class AddressService {
    @Autowired
    AddressRepository addressRepository;

    public List<Address> getAddresss() {
        return addressRepository.findAll();
    }

    public Address getAddress(Integer id) {
        Optional<Address> byId = addressRepository.findById(id);
        return byId.orElse(null);
    }

    public ApiResponse addAddress(Address address) {
        Address newAddress = new Address();
        newAddress.setStreet(address.getStreet());
        newAddress.setHomeNumber(address.getHomeNumber());
        addressRepository.save(newAddress);
        return new ApiResponse("Address added", true);
    }

    public ApiResponse deleteAddress(Integer id) {
        addressRepository.deleteById(id);
        return new ApiResponse("Address deleted",true);
    }

    public ApiResponse editAddress(Integer id, Address address) {
        Address editAddress = addressRepository.findById(id).orElseThrow(() -> new IllegalStateException("Address not found"));
        editAddress.setStreet(address.getStreet());
        editAddress.setHomeNumber(address.getHomeNumber());
        addressRepository.save(editAddress);
        return new ApiResponse("Address edited",true);
    }
}
