package uz.pdp.task_1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import uz.pdp.task_1.entity.Address;
import uz.pdp.task_1.entity.Company;
import uz.pdp.task_1.payload.ApiResponse;
import uz.pdp.task_1.payload.CompanyDto;
import uz.pdp.task_1.repository.AddressRepository;
import uz.pdp.task_1.repository.CompanyRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class CompanyService {
    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    AddressRepository addressRepository;

    public List<Company> getCompanys() {
        return companyRepository.findAll();
    }

    public Company getCompany(Integer id) {
        Optional<Company> byId = companyRepository.findById(id);
        return byId.orElse(null);
    }

    public ApiResponse addCompany(CompanyDto companyDto) {
        Company company = new Company(companyDto.getCorpName(), companyDto.getDirectorName(), addressRepository.getById(companyDto.getAddressId()));
        companyRepository.save(company);
        return new ApiResponse("Company added", true);
    }

    public ApiResponse deleteCompany(Integer id) {
        companyRepository.deleteById(id);
        return new ApiResponse("Company deleted", true);
    }

    public ApiResponse editCompany(Integer id, CompanyDto companyDto) {
        Optional<Company> byId = companyRepository.findById(id);
        if (byId.isPresent()){
            Company company = byId.get();
            company.setCorpName(companyDto.getCorpName());
            company.setDirectorName(companyDto.getDirectorName());
            company.setAddress(addressRepository.getById(companyDto.getAddressId()));
            companyRepository.save(company);
            return new ApiResponse("Company edited", true);
        }
        return new ApiResponse("Company not found", false);
    }

}
