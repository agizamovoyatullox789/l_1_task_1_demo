package uz.pdp.task_1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task_1.entity.Address;
import uz.pdp.task_1.entity.Department;
import uz.pdp.task_1.payload.ApiResponse;
import uz.pdp.task_1.payload.DepartmentDto;
import uz.pdp.task_1.repository.AddressRepository;
import uz.pdp.task_1.repository.CompanyRepository;
import uz.pdp.task_1.repository.DepartmentRepository;

import java.util.List;
import java.util.Optional;

@Service
public class DepartmentService {
    @Autowired
    DepartmentRepository departmentRepository;
    @Autowired
    CompanyRepository companyRepository;
    public List<Department> getDepartments() {
        return departmentRepository.findAll();
    }

    public Department getDepartment(Integer id) {
        Optional<Department> byId = departmentRepository.findById(id);
        return byId.orElse(null);
    }

    public ApiResponse addDepartment(DepartmentDto departmentDto) {
        Department department=new Department(departmentDto.getName(),companyRepository.getById(departmentDto.getCompanyId()));
        departmentRepository.save(department);
        return new ApiResponse("Department added", true);
    }

    public ApiResponse deleteDepartment(Integer id) {
        companyRepository.deleteById(id);
        return new ApiResponse("Department deleted",true);
    }

    public ApiResponse editDepartment(Integer id, DepartmentDto departmentDto) {
        Department department = departmentRepository.findById(id).orElseThrow(() -> new IllegalStateException("Department not found"));
        department.setName(departmentDto.getName());
        department.setCompany(companyRepository.getById(departmentDto.getCompanyId()));
        return new ApiResponse("Department edited",true);
    }
}
