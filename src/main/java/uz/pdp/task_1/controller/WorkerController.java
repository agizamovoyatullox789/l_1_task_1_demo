package uz.pdp.task_1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task_1.entity.Address;
import uz.pdp.task_1.entity.Worker;
import uz.pdp.task_1.payload.ApiResponse;
import uz.pdp.task_1.payload.WorkerDto;
import uz.pdp.task_1.service.AddressService;
import uz.pdp.task_1.service.WorkerService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/workers")
public class WorkerController {
    @Autowired
    WorkerService workerService;

    @GetMapping
    public ResponseEntity<List<Worker>> getWorkers(){
        List<Worker> addresss = workerService.getWorkers();
        return ResponseEntity.ok(addresss);
    }

    @GetMapping("/{id}")
    public Worker getWorker(@PathVariable Integer id){
        return workerService.getWorker(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deleteWorker(@PathVariable Integer id){
        ApiResponse apiResponce = workerService.deleteWorker(id);
        return ResponseEntity.status(apiResponce.isSucces()?204:409).body(apiResponce);
    }

    @PostMapping
    public ResponseEntity<ApiResponse> addWorker(@RequestBody WorkerDto workerDto){
        ApiResponse apiResponce = workerService.addWorker(workerDto);
        if (apiResponce.isSucces()){
            return ResponseEntity.status(201).body(apiResponce);
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(apiResponce);
    }
    @PutMapping("/{id}")
    public ResponseEntity<ApiResponse> editWorker(@PathVariable Integer id, @RequestBody WorkerDto workerDto){
        ApiResponse apiResponce = workerService.editWorker(id, workerDto);
        return ResponseEntity.status(apiResponce.isSucces()?HttpStatus.ACCEPTED:HttpStatus.CONFLICT).body(apiResponce);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMassage = error.getDefaultMessage();
            errors.put(fieldName, errorMassage);
        });
        return errors;
    }

}
