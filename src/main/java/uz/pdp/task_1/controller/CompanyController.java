package uz.pdp.task_1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task_1.entity.Address;
import uz.pdp.task_1.entity.Company;
import uz.pdp.task_1.payload.ApiResponse;
import uz.pdp.task_1.payload.CompanyDto;
import uz.pdp.task_1.service.AddressService;
import uz.pdp.task_1.service.CompanyService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/company")
public class CompanyController {
    @Autowired
    CompanyService companyService;

    @GetMapping
    public ResponseEntity<List<Company>> getCompanys(){
        List<Company> companys = companyService.getCompanys();
        return ResponseEntity.ok(companys);
    }

    @GetMapping("/{id}")
    public Company getCompany(@PathVariable Integer id){
        return companyService.getCompany(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deleteCompany(@PathVariable Integer id){
        ApiResponse apiResponce = companyService.deleteCompany(id);
        return ResponseEntity.status(apiResponce.isSucces()?204:409).body(apiResponce);
    }

    @PostMapping
    public ResponseEntity<ApiResponse> addCompany(@Valid @RequestBody CompanyDto companyDto){
        ApiResponse apiResponce = companyService.addCompany(companyDto);
        if (apiResponce.isSucces()){
            return ResponseEntity.status(201).body(apiResponce);
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(apiResponce);
    }
    @PutMapping("/{id}")
    public ResponseEntity<ApiResponse> editCompany(@PathVariable Integer id,@Valid @RequestBody CompanyDto companyDto){
        ApiResponse apiResponce = companyService.editCompany(id, companyDto);
        return ResponseEntity.status(apiResponce.isSucces()?HttpStatus.ACCEPTED:HttpStatus.CONFLICT).body(apiResponce);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMassage = error.getDefaultMessage();
            errors.put(fieldName, errorMassage);
        });
        return errors;
    }
}
