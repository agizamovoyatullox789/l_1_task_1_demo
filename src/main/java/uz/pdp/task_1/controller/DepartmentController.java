package uz.pdp.task_1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task_1.entity.Address;
import uz.pdp.task_1.entity.Department;
import uz.pdp.task_1.payload.ApiResponse;
import uz.pdp.task_1.payload.DepartmentDto;
import uz.pdp.task_1.service.AddressService;
import uz.pdp.task_1.service.DepartmentService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/department")
public class DepartmentController {
    @Autowired
    DepartmentService departmentService;

    @GetMapping
    public ResponseEntity<List<Department>> getDepartments(){
        List<Department> departments = departmentService.getDepartments();
        return ResponseEntity.ok(departments);
    }

    @GetMapping("/{id}")
    public Department getDepartment(@PathVariable Integer id){
        return departmentService.getDepartment(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deleteDepartment(@PathVariable Integer id){
        ApiResponse apiResponce = departmentService.deleteDepartment(id);
        return ResponseEntity.status(apiResponce.isSucces()?204:409).body(apiResponce);
    }

    @PostMapping
    public ResponseEntity<ApiResponse> addDepartment(@RequestBody DepartmentDto departmentDto){
        ApiResponse apiResponce = departmentService.addDepartment(departmentDto);
        if (apiResponce.isSucces()){
            return ResponseEntity.status(201).body(apiResponce);
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(apiResponce);
    }
    @PutMapping("/{id}")
    public ResponseEntity<ApiResponse> editDepartment(@PathVariable Integer id, @RequestBody DepartmentDto departmentDto){
        ApiResponse apiResponce = departmentService.editDepartment(id,departmentDto);
        return ResponseEntity.status(apiResponce.isSucces()?HttpStatus.ACCEPTED:HttpStatus.CONFLICT).body(apiResponce);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMassage = error.getDefaultMessage();
            errors.put(fieldName, errorMassage);
        });
        return errors;
    }

}
