package uz.pdp.task_1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task_1.entity.Address;
import uz.pdp.task_1.payload.ApiResponse;
import uz.pdp.task_1.service.AddressService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/address")
public class AddressController {
    @Autowired
    AddressService addressService;

    @GetMapping
    public ResponseEntity<List<Address>> getAddresss(){
        List<Address> addresss = addressService.getAddresss();
        return ResponseEntity.ok(addresss);
    }

    @GetMapping("/{id}")
    public Address getAddress(@PathVariable Integer id){
        return addressService.getAddress(id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deleteAddress(@PathVariable Integer id){
        ApiResponse apiResponce = addressService.deleteAddress(id);
        return ResponseEntity.status(apiResponce.isSucces()?204:409).body(apiResponce);
    }

    @PostMapping
    public ResponseEntity<ApiResponse> addAddress(@RequestBody Address address){
        ApiResponse apiResponce = addressService.addAddress(address);
        if (apiResponce.isSucces()){
            return ResponseEntity.status(201).body(apiResponce);
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(apiResponce);
    }
    @PutMapping("/{id}")
    public ResponseEntity<ApiResponse> editAddress(@PathVariable Integer id, @RequestBody Address address){
        ApiResponse apiResponce = addressService.editAddress(id, address);
        return ResponseEntity.status(apiResponce.isSucces()?HttpStatus.ACCEPTED:HttpStatus.CONFLICT).body(apiResponce);
    }
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMassage = error.getDefaultMessage();
            errors.put(fieldName, errorMassage);
        });
        return errors;
    }

}
