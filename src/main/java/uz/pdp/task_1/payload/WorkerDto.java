package uz.pdp.task_1.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorkerDto {
    @NotNull(message = "name bo'sh bo'lmasligi kerak")
    private String name;
    @NotNull(message = "Phone number bo'sh bo'lmasligi kerak")
    private String phoneNumber;
    @NotNull(message = "address id bo'sh bo'lmasligi kerak")
    private  Integer addressId;
    @NotNull(message = "department id bo'sh bo'lmasligi kerak")
    private Integer departmentId;
}
